#include "glimac/SDLWindowManager.hpp"
#include <iostream>

namespace glimac {
    
static SDL_Window *window;

SDLWindowManager::SDLWindowManager(uint32_t width, uint32_t height, const char* title) {
    
    // initialisation de SDL
    if(-1 == SDL_Init(SDL_INIT_VIDEO)) {
        std::cerr << SDL_GetError() << std::endl;
        return;
    }
    
    // get the right OpenGL version
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);
    
    // initialisation de la fenêtre
    window = SDL_CreateWindow(
         title,  //titre
         
         // position à l'écran
         SDL_WINDOWPOS_UNDEFINED,
         SDL_WINDOWPOS_UNDEFINED,
         
         // dimentions par défault
         width,
         height,
         
         // Fullscreen ou Flottant (resizable)
         SDL_WINDOW_RESIZABLE | SDL_WINDOW_OPENGL
    );

    if(window == NULL) {
        std::cerr << SDL_GetError() << std::endl;
        return;
    }
    
    // Create the context OpenGL
    SDL_GL_CreateContext(window);
}

SDLWindowManager::~SDLWindowManager() {
    SDL_Quit();
}

bool SDLWindowManager::pollEvent(SDL_Event& e) {
    return SDL_PollEvent(&e);
}

bool SDLWindowManager::isKeyPressed(SDL_Keycode key) const {
    return SDL_GetKeyboardState(nullptr)[key];
}

// button can SDL_BUTTON_LEFT, SDL_BUTTON_RIGHT and SDL_BUTTON_MIDDLE
bool SDLWindowManager::isMouseButtonPressed(uint32_t button) const {
    return SDL_GetMouseState(nullptr, nullptr) & SDL_BUTTON(button);
}

glm::ivec2 SDLWindowManager::getMousePosition() const {
    glm::ivec2 mousePos;
    SDL_GetMouseState(&mousePos.x, &mousePos.y);
    return mousePos;
}

void SDLWindowManager::swapBuffers() {
    SDL_GL_SwapWindow(window);
}

float SDLWindowManager::getTime() const {
    return 0.001f * SDL_GetTicks();
}

}
