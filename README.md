### Requirements
- Homebrew
- Xcode 10 or Xcode 9

Install GLEW and SDL2 with homebrew before doing anything fancy.
`brew install glew sdl2`

Cmake an Xcode project:
`cmake -G "Xcode"`
